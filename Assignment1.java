import java.util.Scanner;
import java.util.InputMismatchException;

public class Assignment1 
{
    public static void main(String[] args) 
    {
        //We declare our variables here that we're going to use later
        Scanner cin = new Scanner(System.in);
        byte width = 0, heigth = 0;
        char symbol = '*';
        boolean loop = true;
        do //Our main get-draw-check loop will be executed indefinitely until the user chooses to quit 
        {
            //Getting the neccessary data from the user to draw the square
            try {
                System.out.println("Enter your width: ");
                width = cin.nextByte();
                System.out.println("Enter your heigth: ");
                heigth = cin.nextByte();
                System.out.println("Enter desired symbol: ");
                symbol = cin.next().charAt(0);
                draw(width,heigth,symbol);  //Draw the square with the provided data
                loop = check(cin);  //Checking if the loop should continue or break
            } catch (InputMismatchException e) {
                System.out.println("Invalid input! Try again.");
                cin.nextLine();
            };
        }
        while (loop);
        cin = null; //Remember to delete our reference so the garbage collector can do its job
    }
    //We draw the square by simply iterating through the width and heigth
    public static void draw(byte width, byte heigth, char symbol)
    {
        for (int h = 0; h < heigth; h++)
        {
            for (int w = 0; w < width; w++)
            {
                if (h == 0 || h == heigth-1) System.out.print(symbol);          //If we're at minimum or max height, we know it's either the top or bottom wall so we draw the entire line as a wall
                else if ((h == 2 || h == heigth-3) && w > 2 && w < width-3) System.out.print(symbol); //If we happen to be 2 columns and rows down from beginning or up from end, we know we can draw the wall of the inner square
                else if (w > 1 && w < width-2 && h > 1 && h < heigth-2) System.out.print((w == 2 || w == width-3) ? symbol : " "); //if we're inside the walls of the outer square, we check if we are at the right column to draw one wall
                else System.out.print((w == 0 || w == width-1) ? symbol : " "); //However, if we're in between max and min height, we instead check if our current column is supposed to be a wall, if it is we draw our wall, otherwise we add empty space
            }
            System.out.print("\n"); //After the current row is finished drawing, we \n to go to the next line
        }
    }
    //Asks the user if they want to restart or quit the application
    public static boolean check(Scanner cin)
    {
        char response;
        System.out.println("Enter q to exit or c to start again: ");
        response = cin.next().charAt(0);
        if (response == 'c') return true;
        else if (response == 'q') return false;
        else return check(cin);
    }
}